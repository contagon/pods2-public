# README #

### MyLibrary ###

MyLibrary is a simple library example that uses the pods2 cmake scripts to build a
pods project that is compatible with modern cmake practices.

### How to build the code? ###

Required third party libs:

* [Cholmod] (Just for an example local depenedency, get it via sudo apt-get install libsuitesparse-dev)

Other third party dependencies will be downloaded and built locally at configure time.

This software is constructed according to the [Pods2 software policies and templates](https://bitbucket.org/jmangelson/pods2),
that extends the original pods policies and templates [defined here](http://sourceforge.net/projects/pods).

To install:

**Local:** `make`. The project will search 4 levels up the tree for
a `build/` directory. If one is not found it will be installed to
`build/` in the local directory.

**System-wide:** `make BUILD_PREFIX=<build_location>` as root.

'make clean' will uninstall installed files and clear the build tree for everything other than the downloaded third party dependencies.

'make clean-all' will additionally clean the downloaded third party dependencies.

### Questions or Contact ###

If you use this code in your research, we would love hear about it. 
In you have any questions, you can contact us via the following email:

Joshua Mangelson
jmangels@andrew.cmu.edu

